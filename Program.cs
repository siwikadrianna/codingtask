using System;
using System.Collections.Generic;
using System.Linq;

namespace CodingTask
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] A = { 2, 3, 9, 2, 5, 1, 3, 7, 10 };
            int[] B = { 2, 1, 3, 4, 3, 10, 6, 6, 1, 7, 10, 10, 10 };
            int[] expectedC = { 2, 9, 2, 5, 7, 10 };

            Dictionary<int, int> countOccurrences = CountOccurrences(B);

            //O(a*b)
            List<int> C = A.Where(p1 => OnlyKeyIfValueInList(
                countOccurrences,
                SieveOfEratosthenes(countOccurrences.Values.Max())
                ).All(p2 => p2 != p1)).ToList();

            // Test
            if (Enumerable.SequenceEqual(C, expectedC))
            {
                Console.WriteLine("passed");
            }
            else
            {
                Console.WriteLine(string.Join(" ", C)+ "\n" + string.Join(" ", expectedC));
            }
        }
        
        private static Dictionary<int, int> CountOccurrences(int[] array)
        {
            /*
             * CountOccurrences({2, 1, 3, 4, 3, 10, 6, 6, 1, 7, 10, 10, 10}) -> 
             * {2:1, 1:2, 3:2, 4:1, 10:4, 6:2, 7:1}
             * O(b) 
             */
            var groups =
            from a in array
            group a by a into g
            select new
            {
                key = g.Key,
                value = g.Count()
            };

            return groups.ToDictionary(g => g.key, g => g.value);
        }
        private static List<int> SieveOfEratosthenes(int n)
        {
            /*
             * Based on Sieve of Eratosthenes from Wikipedia ;)
             * O(sqrt(n) * n)
             */
            List<int> primeNumbers = new List<int>();
            List<int> compsiteNumbers = new List<int>();
            for (int i = 2; i <= Math.Sqrt(n); i++)
            {
                if (!compsiteNumbers.Contains(i))
                {

                    primeNumbers.Add(i);
                    int j = 2 * i;

                    while (j <= n)
                    {
                        compsiteNumbers.Add(j);
                        j += i;
                    }
                }
            }
            return primeNumbers;
        }

        private static List<int> OnlyKeyIfValueInList(Dictionary<int, int> dict, List<int> list)
        {
            /*
             * OnlyKeyIfValueInList({2:1, 1:2, 3:2, 4:1, 10:4, 6:2, 7:1}, [2, 3, 5]) ->
             * {2, 4, 10, 7}
             * O(d*l)
             */
            List<int> reducedKeys = new List<int>();
            foreach (KeyValuePair<int, int> pair in dict)
            {
                if (list.Contains(pair.Value))
                {
                    reducedKeys.Add(pair.Key);
                }
            }
            return reducedKeys;
        }
    }

}
